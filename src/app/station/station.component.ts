import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { Station } from '../station';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-station',
  standalone: true,
  imports: [CommonModule, RouterLink, RouterOutlet],
  template: `
    <section class="listing">
      <img
        src="{{ station.image }}"
        alt="image d'un ciel"
        class="listing-photo"
      />
      <h2 class="listing-heading">Station {{ station.name }}</h2>
      <ul>
        <li>Latiude : {{ station.latitude }}</li>
        <li>Longitude : {{ station.longitude }}</li>
        <li>Elevation : {{ station.elevation }}</li>

        <a [routerLink]="['/details', station.id]">Learn More</a>
      </ul>
    </section>
  `,
  styleUrl: './station.component.css',
})
export class StationComponent {
  @Input() station!: Station;
}
