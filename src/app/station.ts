export interface Station {
  id: string;
  name: string;
  latitude: number;
  longitude: number;
  elevation: number;
  image: string;
  // temperature: number;
  // visibilite: number;
  // pression: number;
  // humidite: number;
  // neige_au_sol: number;
  // nebulosite: number;
  // point_de_rosee: number;
  // vent_moyen: number;
  // vent_rafales: number;
  // vent_direction: number;
  // pluie_3h: number;
  // pluie_1h: number;
}
