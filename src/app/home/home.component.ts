import { StationService } from './../station.service';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Station } from '../station';
import { StationComponent } from '../station/station.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, StationComponent],
  template: `
    <section class='results'>
      <app-station
        *ngFor="let station of stationList"
        [station]="station"
      ></app-station>
    </section>
  `,
  styleUrl: './home.component.css',
})
export class HomeComponent {
  stationList: Station[] = [];

  stationService: StationService = inject(StationService);

  constructor() {
    this.stationService.getAllStations().then((stationList: Station[]) => {
      this.stationList = stationList;
    });
  }
}
