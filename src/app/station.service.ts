import { Injectable } from '@angular/core';
import { Station } from './station';
import axios from 'axios';

@Injectable({
  providedIn: 'root',
})
export class StationService {
   url = 'http://localhost:3000/stations';

  // url =
  //   'https://www.infoclimat.fr/opendata/?method=get&format=json&stations[]=STATIC0196&stations[]=000NX&stations[]=ME134&stations[]=ME129&stations[]=ME135&start=2023-12-02&end=2023-12-04&token=bFHzOSL8FJP8pVVD6v4UelYz8W9Cs4qyTQc4icwCNoGV1BYHbOKg';

  // https://api.gouv.fr/rechercher-api

  // https://www.pexels.com/join/

  async getAllStations() {
    try {
      const response = await axios.get(this.url, {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET',
        },
      });

      console.log(response.data);
      return response.data;
    } catch (error) {
      // Gérer les erreurs ici
      console.error('Erreur lors de la requête :', error);
      throw error; // ou return null; ou une autre logique de gestion d'erreur
    }
  }

  async getStationById(id: number): Promise<Station | undefined> {
    const data = await fetch(`${this.url}/${id}`);
    return (await data.json()) ?? {};
  }

  constructor() {}
}
